simonallen.org
==============

This repository contains the code for the rewrite of simonallen.org. In addition to the website itself, this repository also contains my advanced new CMD designed specifically for personal websites like mine. It allows for dynamic, real-time edits of website content separate from the code of the website. It also allows for models and layouts to be changed and propogated to the user without the user ever needing to refresh.

Eventually, the goal is to split of and completely de-couple the CMD and the website, but for now both will belong in this repository.
