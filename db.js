var app = require('./app.js');
var db = module.exports = require('mongoose');

db.schema = require('./schema.js');
db.imageRegex = /\.(png|jpeg?|gif)$/

db.setup = function() {
	db.connect('mongodb://localhost/simonallen_org');

	db.watcher = require('./watcher.js');

	db.watcher.setup();	
}

db.addFile = function(path) {
	var match = path.match(db.imageRegex);

	if (match != null) {
		console.log(path);
	}

	

	//var project = db.schema.Project({name: 'test'});	

	//project.save(function(err) {
	//	console.log(err);
	//});

	//console.log('add');
}

db.removeFile = function(path) {
	console.log('remove');
}
