var mongoose = require('mongoose');

var schema = module.exports = {}

schema.Link = mongoose.model('Link', {
	text: String,
	url: String,
	description: String,
	tag: String
});

schema.Image = mongoose.model('Image', {
	url: String,
	caption: String,
	tag: String
});

schema.Info = mongoose.model('Info', {
	title: String,
	intro: String,
	contact: String,
	images: [{type: mongoose.Schema.Types.ObjectId, ref: 'Image'}],
	links: [{type: mongoose.Schema.Types.ObjectId, ref: 'Link'}]
});

schema.Project = mongoose.model('Project', {
	name: String,
	languages: [],
	shortDescription: String,
	longDescription: String,
	images: [{type: mongoose.Schema.Types.ObjectId, ref: 'Image'}],
	downloadLinks: [{type: mongoose.Schema.Types.ObjectId, ref: 'Link'}]
});
