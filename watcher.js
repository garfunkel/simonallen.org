var app = require('./app.js');
var watcher = module.exports = require('watchr');
var walk = require('walk');
var path = require('path');

var options = {
	followLinks: false
}

watcher.scan = function() {
	var walker = walk.walk('./pages', options);

	walker.on('file', function(root, stats, next) {
		app.db.addFile(path.join(root, stats.name));

		next();
	});
}

watcher.setup = function(app) {
	watcher.watch({
		path: './pages',
		listeners: {
			change: function(changeType, filePath, stats, prevStats){
				if (changeType in ('create', 'modify')) {
					app.db.addFile(filePath);
				}

				else if (changeType == 'delete') {
					app.db.removeFile(filePath);
				}
			}
		}
	});

	watcher.scan();
}

