var app = require('./app.js');
var views = require('./views.js');

var urls = module.exports = {}

urls.setup = function() {
	app.get('/', views.home);
}
