var app = module.exports = require('express')();
var server = require('http').createServer(app);

app.io = require('socket.io').listen(server);
app.urls = require('./urls.js');
app.db = require('./db.js');

app.urls.setup();
app.db.setup();

app.listen(80);

console.log('Listening on port 80');
